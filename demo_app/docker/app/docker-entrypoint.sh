#!/bin/sh
set -e

if [ "$SYMFONY_ENV" = 'prod' ]; then
  php composer.phar install --prefer-dist --no-dev --no-progress --no-suggest --optimize-autoloader --classmap-authoritative --no-interaction
else
  php composer.phar install --prefer-dist --no-progress --no-suggest --no-interaction
fi

chown -R www-data var

php bin/console doctrine:schema:update --force

apache2-foreground

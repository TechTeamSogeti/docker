<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends \EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController
{
   public function updateEntity($entity)
   {
       $mailer = $this->get('mailer');
       $message = (new \Swift_Message('Hello'))
           ->setFrom('send@example.com')
           ->setTo('recipient@example.com')
           ->setBody(
               $this->renderView(
                   'emails/entityUpdate.html.twig'
               ),
               'text/html'
           )
           ->addPart(
               $this->renderView(
                   'emails/entityUpdate.txt.twig'
               ),
               'text/plain'
           );

       $mailer->send($message);
       parent::updateEntity($entity);
   }
}
